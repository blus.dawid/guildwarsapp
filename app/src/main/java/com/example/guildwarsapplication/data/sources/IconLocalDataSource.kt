package com.example.guildwarsapplication.data.sources

import android.graphics.Bitmap
import com.example.guildwarsapplication.data.model.IconLocalData

interface IconLocalDataSource {
    suspend fun saveIcon(iconLocalData: IconLocalData)
    suspend fun getIcon(externalIconId: Int): Bitmap?
}