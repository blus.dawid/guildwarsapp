package com.example.guildwarsapplication.data.sources.internal

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import com.example.guildwarsapplication.data.model.IconLocalData
import com.example.guildwarsapplication.data.sources.IconLocalDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.FileInputStream
import java.io.FileOutputStream
import javax.inject.Inject

class IconInternalDataSource @Inject constructor(private val context: Context) :
    IconLocalDataSource {
    private val TAG = "ImageInternalDataSourceTag"
    private val imagePrefix = "SkillInfoImage"

    override suspend fun saveIcon(iconLocalData: IconLocalData) {
        withContext(Dispatchers.IO) {
            var fileOutputStream: FileOutputStream?
            runCatching {
                fileOutputStream =
                    context.openFileOutput(
                        getFileName(iconLocalData.externalId),
                        Context.MODE_PRIVATE
                    )
                iconLocalData.icon.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream)
                Log.d(TAG, "Icon saved in internal storage")
                fileOutputStream?.close()
            }.onFailure { throwable ->
                Log.d(TAG, "An error during saving image: ${throwable.message}")
                throw throwable
            }
        }
    }

    override suspend fun getIcon(externalIconId: Int): Bitmap? {
        var icon: Bitmap? = null
        withContext(Dispatchers.IO) {
            var fileInputStream: FileInputStream
            runCatching {
                fileInputStream = context.openFileInput(getFileName(externalIconId))
                icon = BitmapFactory.decodeStream(fileInputStream)
            }.onFailure { throwable ->
                throw throwable
            }
        }
        return icon
    }

    private fun getFileName(externalId: Int) = imagePrefix + externalId
}