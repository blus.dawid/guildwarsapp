package com.example.guildwarsapplication.data.model

import com.google.gson.annotations.SerializedName

data class SkillInfo(
    @SerializedName("icon")
    val iconUrl: String?,
    val id: Int,
    val name: String?,
    val description: String?,
    val cost: Int?,
)