package com.example.guildwarsapplication.data.repository

import android.graphics.Bitmap
import android.util.Log
import com.example.guildwarsapplication.data.api.GuildWarsService
import com.example.guildwarsapplication.data.api.ImageDownloader
import com.example.guildwarsapplication.data.model.IconLocalData
import com.example.guildwarsapplication.data.model.SkillInfo
import com.example.guildwarsapplication.data.sources.IconLocalDataSource
import com.example.guildwarsapplication.data.util.Resource
import com.example.guildwarsapplication.domain.repository.GuildWarsRepository
import com.example.guildwarsapplication.util.extensions.toStringWithoutBrackets
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.cancel
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

private const val skillsPerRequest = 100
private const val maxIoThreadsCount = 64

class GuildWarsRepositoryImpl @Inject constructor(
    private val guildWarsService: GuildWarsService,
    private val iconLocalDataSource: IconLocalDataSource,
    private val imageDownloader: ImageDownloader
) : GuildWarsRepository {
    private val TAG = "GuildWarsRepositoryTag"
    private var savingJob: Job? = null

    override suspend fun getSkills(): Resource<Flow<List<SkillInfo>>> {
        var flow: Flow<List<SkillInfo>>? = null

        val maxIdsCountThatMayBeHandledWithDeferredCalls = maxIoThreadsCount * skillsPerRequest
        try {
            val skillIds = guildWarsService.getSkillIds()
            withContext(Dispatchers.IO) {
                flow = flow {
                    if (skillIds.size >= maxIdsCountThatMayBeHandledWithDeferredCalls) {
                        handleDataFetcinghWithoutDeferredCalls(skillIds)
                        return@flow
                    }
                    val deferredApiCalls = createDeferredCalls(skillIds)
                    deferredApiCalls.forEach {
                        emit(it.await())
                    }
                }
            }
        } catch (exception: Exception) {
            Log.e(TAG, "An error occurred during request: ${exception.message}")
            return Resource.Error(exception.message.toString())
        }
        flow?.let {
            return Resource.Success(it)
        }
        return Resource.Error("Can't retrieve flow")
    }

    override suspend fun getSkillByExternalId(id: Int): Resource<SkillInfo> {
        return try {
            withContext(Dispatchers.IO) {
                Resource.Success(guildWarsService.getSkillDetails(id))
            }
        } catch (exception: Exception) {
            Resource.Error(exception.message.toString())
        }
    }

    override suspend fun saveSkillIcon(url: String, externalIconId: Int): Boolean {
        try {
            coroutineScope {
                savingJob = launch {
                    val imageBitmap =
                        imageDownloader.downloadImage(url) ?: run {
                            this.cancel()
                            throw Exception("Image downloading failed")
                        }
                    iconLocalDataSource.saveIcon(
                        IconLocalData(externalId = externalIconId, icon = imageBitmap)
                    )
                }
            }
        } catch (exception: Exception) {
            return false
        }
        return savingJob?.isCancelled != true
    }

    override suspend fun cancelIconSaving(): Boolean {
        savingJob?.let {
            it.cancel("Saving cancelled by user")
            return true
        }
        return false
    }

    override suspend fun getSavedIcon(externalIconId: Int): Resource<Bitmap?> {
        return try {
            Resource.Success(iconLocalDataSource.getIcon(externalIconId))
        } catch (exception: Exception) {
            Resource.Error(exception.message.toString())
        }
    }

    private suspend fun createDeferredCalls(skillIds: List<Int>): List<Deferred<List<SkillInfo>>> {
        val deferredApiCalls = mutableListOf<Deferred<List<SkillInfo>>>()
        val lastIterationItemsCount = skillIds.size % skillsPerRequest
        coroutineScope {
            for (i in 0..skillIds.size step skillsPerRequest) {
                val ids = if (skillIds.size - i == lastIterationItemsCount) {
                    skillIds.takeLast(lastIterationItemsCount).toStringWithoutBrackets()
                } else {
                    skillIds.subList(i, i + skillsPerRequest).toStringWithoutBrackets()
                }
                deferredApiCalls.add(async(Dispatchers.IO) {
                    guildWarsService.getSkillsByIds(ids)
                })
            }
        }
        return deferredApiCalls
    }

    private suspend fun FlowCollector<List<SkillInfo>>.handleDataFetcinghWithoutDeferredCalls(
        skillIds: ArrayList<Int>
    ) {
        val lastIterationItemsCount = skillIds.size % skillsPerRequest
        for (i in 0..skillIds.size step skillsPerRequest) {
            val ids = if (skillIds.size - i == lastIterationItemsCount) {
                skillIds.takeLast(lastIterationItemsCount).toStringWithoutBrackets()
            } else {
                skillIds.subList(i, i + skillsPerRequest).toStringWithoutBrackets()
            }
            emit(guildWarsService.getSkillsByIds(ids))
        }
    }
}