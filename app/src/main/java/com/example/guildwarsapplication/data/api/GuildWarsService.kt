package com.example.guildwarsapplication.data.api

import com.example.guildwarsapplication.data.model.SkillInfo
import retrofit2.http.GET
import retrofit2.http.Query

interface GuildWarsService {

    @GET("v2/skills")
    suspend fun getSkillIds(): ArrayList<Int>

    @GET("v2/skills")
    suspend fun getSkillDetails(@Query("id") id: Int): SkillInfo

    @GET("v2/skills")
    suspend fun getSkillsByIds(@Query("ids") ids: String): List<SkillInfo>
}