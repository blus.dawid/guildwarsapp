package com.example.guildwarsapplication.data.api

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import javax.inject.Inject

class ImageDownloaderImpl @Inject constructor() : ImageDownloader {
    override suspend fun downloadImage(imageUrl: String): Bitmap? {
        var bitmap: Bitmap? = null
        runCatching {
            withContext(Dispatchers.IO) {
                val url = URL(imageUrl)
                val connection: HttpURLConnection = url
                    .openConnection() as HttpURLConnection
                connection.apply {
                    doInput = true
                    connect()
                }
                val input: InputStream = connection.inputStream
                bitmap = BitmapFactory.decodeStream(input)
            }
        }.onFailure { throwable ->
            Log.d(
                "ImageDownloaderImpl",
                "An error occurred during downloading image: ${throwable.message}"
            )
        }
        return bitmap
    }
}