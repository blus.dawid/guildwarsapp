package com.example.guildwarsapplication.data.model

import android.graphics.Bitmap

data class IconLocalData(
    val externalId: Int,
    val icon: Bitmap
)