package com.example.guildwarsapplication.data.api

import android.graphics.Bitmap

interface ImageDownloader {
    suspend fun downloadImage(imageUrl: String): Bitmap?
}