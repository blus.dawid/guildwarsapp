package com.example.guildwarsapplication

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class GuildWarsApp : Application()