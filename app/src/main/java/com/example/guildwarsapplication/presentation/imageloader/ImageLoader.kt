package com.example.guildwarsapplication.presentation.imageloader

import android.widget.ImageView

interface ImageLoader {
    fun loadImage(imageUrl: String, targetView: ImageView)
}