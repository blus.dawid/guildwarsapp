package com.example.guildwarsapplication.presentation.navigation

import android.os.Bundle
import androidx.navigation.NavController
import javax.inject.Inject

class ScreensNavigatorImpl @Inject constructor(private val navController: NavController) :
    ScreenNavigator {
    override fun navigateTo(destination: Destination, arguments: Bundle?) {
        navController.navigate(destination.navigationActionId, arguments)
    }

    override fun goBack() {
        navController.popBackStack()
    }
}