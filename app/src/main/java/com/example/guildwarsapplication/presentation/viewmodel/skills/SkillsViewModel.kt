package com.example.guildwarsapplication.presentation.viewmodel.skills

import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.guildwarsapplication.data.util.Resource
import com.example.guildwarsapplication.domain.usecase.GetGuildWarsSkillsUseCase
import com.example.guildwarsapplication.domain.usecase.SortSkillsUseCase
import com.example.guildwarsapplication.presentation.DetailsFragment
import com.example.guildwarsapplication.presentation.navigation.NavigationAction
import com.example.guildwarsapplication.util.extensions.SortingOrder
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SkillsViewModel @Inject constructor(
    private val getGuildWarsSkillsUseCase: GetGuildWarsSkillsUseCase,
    private val sortSkillsUseCase: SortSkillsUseCase
) : ViewModel() {

    private val _uiState: MutableStateFlow<SkillsUiState> =
        MutableStateFlow(SkillsUiState())
    val uiState: StateFlow<SkillsUiState> = _uiState.asStateFlow()

    private var sortingOrder: SortingOrder = SortingOrder.ASCENDING

    fun setSkillsSortingOrder(sortingOrder: SortingOrder) {
        this.sortingOrder = sortingOrder
        _uiState.update { currentState ->
            val list = currentState.skillsResource?.data ?: return@update currentState
            currentState.copy(
                skillsResource = Resource.Success(sortSkillsUseCase.execute(list, sortingOrder))
            )
        }
    }

    fun fetchSkills() {
        if (uiState.value.skillsResource is Resource.Success) {
            return
        }
        viewModelScope.launch {
            _uiState.update { currentState -> currentState.copy(skillsResource = Resource.Loading()) }
            when (val resource = getGuildWarsSkillsUseCase.execute()) {
                is Resource.Success -> {
                    _uiState.update { currentState ->
                        currentState.copy(
                            skillsResource = Resource.Success(
                                sortSkillsUseCase.execute(resource.data!!, sortingOrder)
                            )
                        )
                    }
                    return@launch
                }
                else -> {
                    _uiState.update { currentState ->
                        currentState.copy(skillsResource = resource)
                    }
                }
            }
        }
    }

    fun navigateToSkillDetails(skillId: Int) {
        _uiState.update { currentState ->
            val navigationAction = NavigationAction.ToSkillDetails(
                arguments = bundleOf(Pair(DetailsFragment.SKLL_ID_KEY, skillId))
            )
            currentState.copy(navigationAction = navigationAction)
        }
    }

    fun clearNavigationState() {
        _uiState.update { currentState ->
            currentState.copy(navigationAction = null)
        }
    }
}