package com.example.guildwarsapplication.presentation.di

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.guildwarsapplication.presentation.imageloader.GlideImageLoader
import com.example.guildwarsapplication.presentation.imageloader.ImageLoader
import com.example.guildwarsapplication.presentation.navigation.ScreenNavigator
import com.example.guildwarsapplication.presentation.navigation.ScreensNavigatorImpl
import com.example.guildwarsapplication.util.messages.MessageService
import com.example.guildwarsapplication.util.messages.ToastMessageService
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import dagger.hilt.android.scopes.FragmentScoped

@Module
@InstallIn(FragmentComponent::class)
abstract class PresentationModule {

    @Binds
    @FragmentScoped
    abstract fun provideScreenNavigator(screenNavigator: ScreensNavigatorImpl): ScreenNavigator

    @Binds
    @FragmentScoped
    abstract fun provideImageLoader(imageLoader: GlideImageLoader): ImageLoader

    @Binds
    abstract fun provideMessageService(messageService: ToastMessageService): MessageService

    companion object {
        @Provides
        fun provideFragmentActivityContext(fragment: Fragment) = fragment.requireContext()

        @Provides
        fun provideLayoutManager(activity: FragmentActivity): LinearLayoutManager =
            LinearLayoutManager(activity)

        @Provides
        @FragmentScoped
        fun provideNavController(fragment: Fragment) = fragment.findNavController()
    }
}