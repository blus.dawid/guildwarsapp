package com.example.guildwarsapplication.presentation.di

import android.app.Application
import com.example.guildwarsapplication.data.sources.IconLocalDataSource
import com.example.guildwarsapplication.data.sources.internal.IconInternalDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class LocalStorageModule {
    @Provides
    fun provideIconDataSource(context: Application): IconLocalDataSource =
        IconInternalDataSource(context)

}