package com.example.guildwarsapplication.presentation.viewmodel.details

import com.example.guildwarsapplication.data.model.SkillInfo
import com.example.guildwarsapplication.data.util.Resource
import com.example.guildwarsapplication.presentation.navigation.NavigationAction

data class DetailsUiState(
    val skillInfoResource: Resource<SkillInfo>? = null,
    val savingInProgress: Boolean? = null,
    val saveButtonTextId: Int? = null,
    val navigationAction: NavigationAction? = null
)