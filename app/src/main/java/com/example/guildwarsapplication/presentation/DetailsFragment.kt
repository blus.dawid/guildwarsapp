package com.example.guildwarsapplication.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.navArgs
import com.example.guildwarsapplication.R
import com.example.guildwarsapplication.data.model.SkillInfo
import com.example.guildwarsapplication.data.util.Resource
import com.example.guildwarsapplication.databinding.FragmentDetailsBinding
import com.example.guildwarsapplication.presentation.imageloader.ImageLoader
import com.example.guildwarsapplication.presentation.navigation.NavigationAction
import com.example.guildwarsapplication.presentation.navigation.ScreenNavigator
import com.example.guildwarsapplication.presentation.viewmodel.MainActivityViewModel
import com.example.guildwarsapplication.presentation.viewmodel.details.DetailsViewModel
import com.example.guildwarsapplication.util.messages.MessageService
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class DetailsFragment : Fragment() {
    private val detailsViewModel: DetailsViewModel by viewModels()
    private val mainActivityViewModel: MainActivityViewModel by activityViewModels()
    private var _binding: FragmentDetailsBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var screenNavigator: ScreenNavigator

    @Inject
    lateinit var imageLoader: ImageLoader

    @Inject
    lateinit var messageService: MessageService

    companion object {
        const val SKLL_ID_KEY = "skill_id"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DataBindingUtil.inflate(inflater, R.layout.fragment_details, container, false)
        binding.apply {
            viewModel = detailsViewModel
            mainViewModel = mainActivityViewModel
            lifecycleOwner = viewLifecycleOwner
        }
        observeChanges()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpToolbar()
        val args: DetailsFragmentArgs by navArgs()
        detailsViewModel.fetchSkillInfo(args.skillId)
    }

    private fun observeChanges() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                detailsViewModel.uiState.collect { state ->
                    state.navigationAction?.let { navAction ->
                        when (navAction) {
                            is NavigationAction.GoBack -> screenNavigator.goBack()
                            else -> screenNavigator.navigateTo(
                                navAction.destination!!,
                                navAction.arguments
                            )
                        }
                    }
                    handleProgressBarVisibility(
                        savingInProgress = state.savingInProgress == true,
                        resourceIsLoading = state.skillInfoResource is Resource.Loading
                    )
                    handleButtonState(
                        buttonTextId = state.saveButtonTextId,
                        shouldBeShown = state.skillInfoResource is Resource.Success
                    )
                    handleSkillInfo(state.skillInfoResource)
                }
            }
        }
    }

    private fun handleButtonState(buttonTextId: Int?, shouldBeShown: Boolean) {
        binding.button.apply {
            isVisible = shouldBeShown
            buttonTextId ?: return
            text = resources.getText(buttonTextId)
        }
    }

    private fun handleProgressBarVisibility(
        savingInProgress: Boolean,
        resourceIsLoading: Boolean
    ) {
        binding.progressBar.isVisible = savingInProgress || resourceIsLoading
    }

    private fun handleSkillInfo(
        skillInfoResource: Resource<SkillInfo>?,
    ) {
        skillInfoResource ?: return
        when (skillInfoResource) {
            is Resource.Success -> {
                val skillInfo = skillInfoResource.data ?: return
                binding.apply {
                    skillInfo.cost?.let {
                        costTextView.text = String.format(
                            resources.getText(R.string.skill_cost_text).toString(), it
                        )
                    }
                    imageLoader.loadImage(skillInfo.iconUrl!!, skillIcon)
                }
            }
            is Resource.Error -> {
                skillInfoResource.message?.let { message ->
                    messageService.showMessage(message)
                }
            }
            is Resource.Loading -> {
                return
            }
        }
    }

    private fun setUpToolbar() {
        binding.toolbar.apply {
            setNavigationIcon(androidx.appcompat.R.drawable.abc_ic_ab_back_material)
            setNavigationOnClickListener {
                detailsViewModel.navigateToPreviousScreen()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        detailsViewModel.clearNavigationState()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}