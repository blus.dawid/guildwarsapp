package com.example.guildwarsapplication.presentation.viewmodel.details

import android.graphics.Bitmap
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.guildwarsapplication.R
import com.example.guildwarsapplication.data.util.Resource
import com.example.guildwarsapplication.domain.usecase.CancelSavingIconUseCase
import com.example.guildwarsapplication.domain.usecase.GetIconUseCase
import com.example.guildwarsapplication.domain.usecase.GetSingleSkillInfoUseCase
import com.example.guildwarsapplication.domain.usecase.SaveIconUseCase
import com.example.guildwarsapplication.presentation.navigation.NavigationAction
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailsViewModel @Inject constructor(
    private val getSingleSkillInfoUseCase: GetSingleSkillInfoUseCase,
    private val cancelSavingIconUseCase: CancelSavingIconUseCase,
    private val saveIconUseCase: SaveIconUseCase,
    private val getIconUseCase: GetIconUseCase
) : ViewModel() {

    private val _uiState: MutableStateFlow<DetailsUiState> = MutableStateFlow(DetailsUiState())
    val uiState: StateFlow<DetailsUiState> = _uiState.asStateFlow()

    fun fetchSkillInfo(id: Int) {
        if (uiState.value.skillInfoResource is Resource.Success) {
            return
        }
        viewModelScope.launch {
            _uiState.update { currentState ->
                currentState.copy(skillInfoResource = Resource.Loading())
            }
            _uiState.update { currentState ->
                currentState.copy(skillInfoResource = getSingleSkillInfoUseCase.execute(id))
            }
            if (iconAlreadySaved()) {
                _uiState.update { currentState -> currentState.copy(saveButtonTextId = R.string.already_saved_text) }
            }
        }
    }

    private suspend fun iconAlreadySaved(): Boolean {
        var icon: Bitmap? = null
        if (uiState.value.skillInfoResource is Resource.Success) {
            val id = uiState.value.skillInfoResource?.data?.id ?: return false
            icon = getIconUseCase.execute(id).data
        }
        return icon != null
    }

    private fun saveIcon() {
        uiState.value.skillInfoResource ?: return
        viewModelScope.launch {
            _uiState.update { currentState ->
                currentState.copy(
                    savingInProgress = true,
                    saveButtonTextId = R.string.cancel_text
                )
            }

            val skillInfo = uiState.value.skillInfoResource ?: return@launch
            val saveSucceeded = saveIconUseCase.execute(
                url = skillInfo.data!!.iconUrl!!,
                externalIconId = skillInfo.data.id
            )

            _uiState.update { currentState ->
                currentState.copy(
                    savingInProgress = false,
                    saveButtonTextId = if (saveSucceeded) {
                        R.string.already_saved_text
                    } else {
                        R.string.save_text
                    }
                )
            }
        }
    }

    private fun cancelIconSaving() {
        if (uiState.value.savingInProgress == false) {
            return
        }
        viewModelScope.launch {
            cancelSavingIconUseCase.execute()
        }
    }

    fun saveIconOrCancelSaving() {
        if (uiState.value.savingInProgress == true) {
            cancelIconSaving()
            return
        }
        viewModelScope.launch {
            if (iconAlreadySaved()) {
                return@launch
            }
            saveIcon()
        }
    }

    fun navigateToPreviousScreen() {
        _uiState.update { currentState ->
            currentState.copy(navigationAction = NavigationAction.GoBack)
        }
    }

    fun clearNavigationState() {
        _uiState.update { currentState ->
            currentState.copy(navigationAction = null)
        }
    }
}