package com.example.guildwarsapplication.presentation.di

import com.example.guildwarsapplication.data.repository.GuildWarsRepositoryImpl
import com.example.guildwarsapplication.domain.repository.GuildWarsRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {

    @Binds
    @Singleton
    fun provideGuildWarsRepository(guildWarsRepositoryImpl: GuildWarsRepositoryImpl): GuildWarsRepository
}