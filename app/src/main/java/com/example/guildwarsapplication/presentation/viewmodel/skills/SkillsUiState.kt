package com.example.guildwarsapplication.presentation.viewmodel.skills

import com.example.guildwarsapplication.data.model.SkillInfo
import com.example.guildwarsapplication.data.util.Resource
import com.example.guildwarsapplication.presentation.navigation.NavigationAction

data class SkillsUiState(
    val skillsResource: Resource<List<SkillInfo>>? = null,
    val navigationAction: NavigationAction? = null
)