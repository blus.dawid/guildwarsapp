package com.example.guildwarsapplication.presentation.navigation

import android.os.Bundle

sealed class NavigationAction(
    val destination: Destination? = null,
    val arguments: Bundle? = null
) {
    class ToSkillDetails(arguments: Bundle) :
        NavigationAction(Destination.DETAILS_SCREEN, arguments)

    object GoBack : NavigationAction()
}