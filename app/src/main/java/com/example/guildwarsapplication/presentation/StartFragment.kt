package com.example.guildwarsapplication.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.guildwarsapplication.R
import com.example.guildwarsapplication.data.util.Resource
import com.example.guildwarsapplication.databinding.FragmentStartBinding
import com.example.guildwarsapplication.presentation.adapter.SkillsAdapter
import com.example.guildwarsapplication.presentation.navigation.NavigationAction
import com.example.guildwarsapplication.presentation.navigation.ScreenNavigator
import com.example.guildwarsapplication.presentation.viewmodel.MainActivityViewModel
import com.example.guildwarsapplication.presentation.viewmodel.skills.SkillsViewModel
import com.example.guildwarsapplication.util.extensions.SortingOrder
import com.example.guildwarsapplication.util.messages.MessageService
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Provider

@AndroidEntryPoint
class StartFragment : Fragment() {
    private var _binding: FragmentStartBinding? = null
    private val binding get() = _binding!!
    private val skillsViewModel: SkillsViewModel by viewModels()
    private val mainActivityViewModel: MainActivityViewModel by activityViewModels()

    @Inject
    lateinit var layoutManager: Provider<LinearLayoutManager>

    @Inject
    lateinit var skillsAdapter: SkillsAdapter

    @Inject
    lateinit var screenNavigator: ScreenNavigator

    @Inject
    lateinit var messageService: MessageService

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DataBindingUtil.inflate(inflater, R.layout.fragment_start, container, false)
        binding.apply {
            lifecycleOwner = viewLifecycleOwner
            viewModel = skillsViewModel
            mainViewModel = mainActivityViewModel
        }
        initRecyclerView()
        initToolbarListener()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeChanges()
        skillsViewModel.fetchSkills()
    }

    private fun observeChanges() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                skillsViewModel.uiState.collect { state ->
                    state.navigationAction?.let { navAction ->
                        when (navAction) {
                            is NavigationAction.GoBack -> {
                                screenNavigator.goBack()
                                return@collect
                            }
                            else -> screenNavigator.navigateTo(
                                navAction.destination!!,
                                navAction.arguments
                            )
                        }
                    }
                    val resource = state.skillsResource ?: return@collect
                    when (resource) {
                        is Resource.Loading -> {
                            hideRetryButton()
                            showProgressBar()
                        }
                        is Resource.Success -> {
                            skillsAdapter.apply { setList(resource.data!!) }
                            hideRetryButton()
                            hideProgressBar()
                        }
                        is Resource.Error -> {
                            resource.message?.let { message ->
                                messageService.showMessage(message)

                            }
                            hideProgressBar()
                            showRetryButton()
                        }
                    }
                }
            }
        }
    }

    private fun initRecyclerView() {
        binding.skillsRecyclerView.also {
            it.adapter = skillsAdapter.also { adapter ->
                adapter.setOnItemClickListener { skillInfo ->
                    skillsViewModel.navigateToSkillDetails(skillInfo.id)
                }
            }
            it.layoutManager = layoutManager.get()
        }
    }

    private fun initToolbarListener() {
        val changeSortingOrder = { item: MenuItem, order: SortingOrder ->
            item.isChecked = !item.isChecked
            skillsViewModel.setSkillsSortingOrder(order)
            true
        }
        binding.toolbar.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.sort_ascending -> {
                    changeSortingOrder(item, SortingOrder.ASCENDING)
                }
                R.id.sort_descending -> {
                    changeSortingOrder(item, SortingOrder.DESCENDING)
                }
                else -> false
            }
        }
    }

    private fun hideProgressBar() {
        binding.progressBar.isVisible = false
    }

    private fun showProgressBar() {
        binding.progressBar.isVisible = true
    }

    private fun hideRetryButton() {
        binding.retryButton.isVisible = false
    }

    private fun showRetryButton() {
        binding.retryButton.isVisible = true
    }

    override fun onStop() {
        super.onStop()
        skillsViewModel.clearNavigationState()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}