package com.example.guildwarsapplication.presentation.imageloader

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import dagger.hilt.android.scopes.FragmentScoped
import javax.inject.Inject

@FragmentScoped
class GlideImageLoader @Inject constructor(private val context: Context) : ImageLoader {
    override fun loadImage(imageUrl: String, targetView: ImageView) {
        Glide.with(context).load(imageUrl).into(targetView)
    }
}