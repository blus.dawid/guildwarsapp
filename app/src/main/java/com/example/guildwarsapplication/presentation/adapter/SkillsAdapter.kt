package com.example.guildwarsapplication.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.guildwarsapplication.data.model.SkillInfo
import com.example.guildwarsapplication.databinding.SkillsListItemBinding
import com.example.guildwarsapplication.presentation.imageloader.ImageLoader
import javax.inject.Inject

class SkillsAdapter @Inject constructor(private val imageLoader: ImageLoader) :
    RecyclerView.Adapter<SkillsAdapter.SkillsViewHolder>() {

    private var skills: MutableList<SkillInfo> = mutableListOf()

    private var onItemClickListener: ((SkillInfo) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SkillsViewHolder {
        val binding =
            SkillsListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SkillsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SkillsViewHolder, position: Int) {
        holder.bind(skills[position])
    }

    override fun getItemCount(): Int {
        return skills.size
    }

    fun setList(skills: List<SkillInfo>) {
        this.skills.apply {
            clear()
            addAll(skills)
            notifyDataSetChanged()
        }
    }

    fun setOnItemClickListener(listener: (SkillInfo) -> Unit) {
        onItemClickListener = listener
    }

    inner class SkillsViewHolder(private val binding: SkillsListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(skill: SkillInfo) {
            binding.apply {
                skillName.text = skill.name

                imageLoader.loadImage(skill.iconUrl!!, icon)

                root.setOnClickListener {
                    onItemClickListener?.invoke(skill)
                }
            }
        }
    }
}