package com.example.guildwarsapplication.presentation.di

import com.example.guildwarsapplication.BuildConfig
import com.example.guildwarsapplication.data.api.ImageDownloader
import com.example.guildwarsapplication.data.api.ImageDownloaderImpl
import com.example.guildwarsapplication.data.api.GuildWarsService
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class NetModule {

    @Binds
    @Singleton
    abstract fun provideImageDownloader(imageDownloaderImpl: ImageDownloaderImpl): ImageDownloader

    companion object {
        @Provides
        @Singleton
        fun provideRetrofit(): Retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BuildConfig.GUILD_WARS_URL)
            .build()

        @Provides
        @Singleton
        fun provideGuildWarsApi(retrofit: Retrofit): GuildWarsService =
            retrofit.create(GuildWarsService::class.java)
    }
}