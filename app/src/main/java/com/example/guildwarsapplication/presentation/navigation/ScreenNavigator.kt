package com.example.guildwarsapplication.presentation.navigation

import android.os.Bundle
import com.example.guildwarsapplication.R

interface ScreenNavigator {
    fun navigateTo(destination: Destination, arguments: Bundle? = null)
    fun goBack()
}

enum class Destination(val navigationActionId: Int) {
    DETAILS_SCREEN(R.id.detailsFragment)
}