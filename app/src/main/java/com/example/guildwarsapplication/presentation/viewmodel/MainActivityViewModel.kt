package com.example.guildwarsapplication.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor() : ViewModel() {

    private val _isDarkTheme: MutableLiveData<Boolean> = MutableLiveData(false)
    val isDarkTheme = _isDarkTheme

    fun setDarkTheme(setDarkTheme: Boolean) {
        _isDarkTheme.value = setDarkTheme
    }
}