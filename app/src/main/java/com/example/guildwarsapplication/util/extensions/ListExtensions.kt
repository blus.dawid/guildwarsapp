package com.example.guildwarsapplication.util.extensions

import com.example.guildwarsapplication.data.model.SkillInfo

fun <T> List<T>.toStringWithoutBrackets(): String {
    return toString().replace("[", "").replace("]", "")
}

fun List<SkillInfo>.sort(sortOrder: SortingOrder): List<SkillInfo> {
    return when (sortOrder) {
        SortingOrder.ASCENDING -> {
            sortedBy { it.name }
        }
        SortingOrder.DESCENDING -> {
            sortedByDescending { it.name }
        }
    }
}

enum class SortingOrder {
    ASCENDING, DESCENDING
}