package com.example.guildwarsapplication.util.messages

interface MessageService {
    fun showMessage(message: String)
}