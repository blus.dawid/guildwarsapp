package com.example.guildwarsapplication.util.messages

import android.content.Context
import android.widget.Toast
import javax.inject.Inject

class ToastMessageService @Inject constructor(private val context: Context) : MessageService {
    override fun showMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }
}