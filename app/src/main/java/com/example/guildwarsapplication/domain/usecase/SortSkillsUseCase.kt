package com.example.guildwarsapplication.domain.usecase

import com.example.guildwarsapplication.data.model.SkillInfo
import com.example.guildwarsapplication.util.extensions.SortingOrder
import com.example.guildwarsapplication.util.extensions.sort
import javax.inject.Inject

class SortSkillsUseCase @Inject constructor() {
    fun execute(skills: List<SkillInfo>, sortingOrder: SortingOrder): List<SkillInfo> {
        return skills.sort(sortingOrder)
    }
}