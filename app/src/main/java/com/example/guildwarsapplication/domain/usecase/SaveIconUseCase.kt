package com.example.guildwarsapplication.domain.usecase

import com.example.guildwarsapplication.domain.repository.GuildWarsRepository
import javax.inject.Inject

class SaveIconUseCase @Inject constructor(
    private val guildWarsRepositoryImpl: GuildWarsRepository
) {
    suspend fun execute(url: String, externalIconId: Int): Boolean {
        return guildWarsRepositoryImpl.saveSkillIcon(url, externalIconId)
    }
}