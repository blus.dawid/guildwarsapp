package com.example.guildwarsapplication.domain.usecase

import com.example.guildwarsapplication.domain.repository.GuildWarsRepository
import javax.inject.Inject

class CancelSavingIconUseCase @Inject constructor(
    private val guildWarsRepository: GuildWarsRepository
) {
    suspend fun execute() {
        guildWarsRepository.cancelIconSaving()
    }
}