package com.example.guildwarsapplication.domain.usecase

import com.example.guildwarsapplication.data.model.SkillInfo
import com.example.guildwarsapplication.data.util.Resource
import com.example.guildwarsapplication.domain.repository.GuildWarsRepository
import javax.inject.Inject

class GetGuildWarsSkillsUseCase @Inject constructor(private val repository: GuildWarsRepository) {
    suspend fun execute(): Resource<List<SkillInfo>> {
        val skillsInfo = mutableListOf<SkillInfo>()
        val repositoryResource = repository.getSkills()
        repositoryResource.data?.collect { skills ->
            val filteredSkills = skills.filter {
                !it.name.isNullOrEmpty() && !it.iconUrl.isNullOrEmpty()
            }
            skillsInfo.addAll(filteredSkills)
        } ?: return Resource.Error(repositoryResource.message)
        return Resource.Success(skillsInfo)
    }
}