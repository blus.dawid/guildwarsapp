package com.example.guildwarsapplication.domain.repository

import android.graphics.Bitmap
import com.example.guildwarsapplication.data.model.SkillInfo
import com.example.guildwarsapplication.data.util.Resource
import kotlinx.coroutines.flow.Flow

interface GuildWarsRepository {
    suspend fun getSkills(): Resource<Flow<List<SkillInfo>>>
    suspend fun getSkillByExternalId(id: Int): Resource<SkillInfo>
    suspend fun saveSkillIcon(url: String, externalIconId: Int): Boolean
    suspend fun cancelIconSaving(): Boolean
    suspend fun getSavedIcon(externalIconId: Int): Resource<Bitmap?>
}