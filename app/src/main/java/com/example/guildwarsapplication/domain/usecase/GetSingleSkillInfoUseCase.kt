package com.example.guildwarsapplication.domain.usecase

import com.example.guildwarsapplication.data.model.SkillInfo
import com.example.guildwarsapplication.data.util.Resource
import com.example.guildwarsapplication.domain.repository.GuildWarsRepository
import javax.inject.Inject

class GetSingleSkillInfoUseCase @Inject constructor(
    private val guildWarsRepository: GuildWarsRepository
) {
    suspend fun execute(id: Int): Resource<SkillInfo> {
        return guildWarsRepository.getSkillByExternalId(id)
    }
}