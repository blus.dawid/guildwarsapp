package com.example.guildwarsapplication.domain.usecase

import android.graphics.Bitmap
import com.example.guildwarsapplication.data.util.Resource
import com.example.guildwarsapplication.domain.repository.GuildWarsRepository
import javax.inject.Inject

class GetIconUseCase @Inject constructor(
    private val repository: GuildWarsRepository
) {
    suspend fun execute(externalIconId: Int): Resource<Bitmap?> {
        return repository.getSavedIcon(externalIconId)
    }
}